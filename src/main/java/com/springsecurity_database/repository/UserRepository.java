package com.springsecurity_database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springsecurity_database.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> 
{

	User findByUsername(String username);
    
	User getUserByUsername(String username);
}
