package com.springsecurity_database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springsecurity_database.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

}
