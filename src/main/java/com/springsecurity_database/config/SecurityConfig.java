package com.springsecurity_database.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
    DataSource dataSource;

	 @Bean
	    public BCryptPasswordEncoder encodePWD()
	    {
	    	System.out.println("Hello");
	    	return new BCryptPasswordEncoder();
	    }
        
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		System.out.println("hii");
		auth.userDetailsService(userDetailsService)
		.passwordEncoder(encodePWD());
		
		auth.jdbcAuthentication()
		
				
				  .authoritiesByUsernameQuery("select u.username, r.role from user u inner join role ur on (u.user_id = ur.role_id)"
				  
				  + " inner join role r on (ur.role_id=r.role_id) where u.username=?")
				 
		.usersByUsernameQuery("select username,password,1 from user u where username=?")
		.dataSource(dataSource);
	}
	
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
    	http.csrf().disable();
    	http.authorizeRequests().antMatchers("/rest/**").permitAll()
    	    .and().authorizeRequests().antMatchers("/secure/**").authenticated().anyRequest().hasAnyRole("ADMIN")
    	    .and()
    	    .formLogin().loginPage("/rest/login").usernameParameter("username").passwordParameter("password").permitAll()
    	    .loginProcessingUrl("/loginProcess")
    	    .failureForwardUrl("/failure")
    	    .defaultSuccessUrl("/",true).permitAll();
    	
    }
    
   
}
