package com.springsecurity_database.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.springsecurity_database.model.User;
import com.springsecurity_database.repository.UserRepository;

@RestController
@RequestMapping("/rest")
public class AdminController 
{
     @Autowired
     private UserRepository userRepository;
     
     @Autowired
     private BCryptPasswordEncoder passwordEncoder;
     
	/* @PreAuthorize("hasAnyRole('ADMIN')") */
     @PostMapping("/admin/add")
     public ResponseEntity addUserByAdmin(@RequestBody User user)
     {
    	 String pwd=user.getPassword();
    	 String encryptPwd=passwordEncoder.encode(pwd);
    	 user.setPassword(encryptPwd);
    	 System.out.print("save");
    	 userRepository.save(user);
    	
    	 return new ResponseEntity("user added successfully....", HttpStatus.OK);
     }
     
     @GetMapping("/login")
     public ModelAndView login(Model model)
     {
    	 
     	 return new ModelAndView("login");
     	 
     }
     
     @GetMapping("/failure")
     public ModelAndView failure(Model model)
     {
    	 
    	 return new ModelAndView("failure");
     }
     @GetMapping("/")
     public ModelAndView Welcome(Model model)
     {
    	 
    	 return new ModelAndView("welcome");
     }
}
