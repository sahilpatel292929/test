package com.springsecurity_database.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/avi")
public class AdminC {
	@GetMapping("/login")
    public ModelAndView login(Model model)
    {
   	 
    	 return new ModelAndView("login");
    	 
    }
    
    @GetMapping("/failure")
    public ModelAndView failure(Model model)
    {
   	 
   	 return new ModelAndView("failure");
    }
    @GetMapping("/")
    public ModelAndView Welcome(Model model)
    {
   	 
   	 return new ModelAndView("welcome");
    }
}
