package com.springsecurity_database.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/auth")
public class ApplicationController
{
	  
	        @GetMapping("/process")
            public ResponseEntity process()
            {
            	return new ResponseEntity("Processing", HttpStatus.OK);
            }
}
