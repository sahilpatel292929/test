package com.springsecurity_database.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springsecurity_database.model.User;
import com.springsecurity_database.repository.UserRepository;

@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService
{
	@Autowired
	private UserRepository userRepository;
	
	@Override
     public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
     {
    	 User user=userRepository.findByUsername(username);
    	 CustomUserDetails userDetails = null;
    	 if(user!=null)
    	 {
    		 userDetails = new CustomUserDetails();
    		   userDetails.setUser(user);
    	 }
    	 else
    	 {
    		 throw new UsernameNotFoundException("User not exist"+username);
    	 }
		
		  User user1=userRepository.getUserByUsername(username);
		  CustomUserDetails userDetails1 = null; 
		  if(user!=null)
	    	 {
	    		 userDetails1 = new CustomUserDetails();
	    		   userDetails1.setUser(user1);
	    	 }
	    	 else
	    	 {
	    		 throw new UsernameNotFoundException("User not exist"+username);
	    	 }
		 
    	return userDetails;	 
     }
}
